import React from 'react';
import {View} from 'react-native';
import PreLoader from "./application/components/PreLoader";
import BackgroundImage from "./application/components/BackgroundImage";
import AppButton from "./application/components/AppButton";

//import GuestNavigation from './application/navigations/guest';
//import LoggedNavigation from './application/navigations/logged';

export default class App extends React.Component {
  render() {
    return (
      <BackgroundImage source={require('./assets/imagenes/fondo.png')}>
				<View style={{justifyContent: 'center', flex: 1}}>
					<AppButton
						bgColor="rgba(111, 38, 74, 0.7)"
						title="Entrar"
						action={() => console.log(1)}
						iconName="sign-in"
						iconSize={30}
						iconColor="#fff"
					/>
					<AppButton
						bgColor="rgba(200, 200, 50, 0.7)"
						title="Regístrarme"
						action={() => console.log(2)}
						iconName="user-plus"
						iconSize={30}
						iconColor="#fff"
					/>
					<AppButton
						bgColor="rgba(67, 67, 146, 0.7)"
						title="Facebook"
						action={() => console.log(3)}
						iconName="facebook"
						iconSize={30}
						iconColor="#fff"
					/>
				</View>
			</BackgroundImage>
    );
  }
}
