import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {SocialLoginModule, AuthServiceConfig, GoogleLoginProvider,FacebookLoginProvider} from 'ng4-social-login';
const config = new AuthServiceConfig([
{
  id: GoogleLoginProvider.PROVIDER_ID,
  provider: new GoogleLoginProvider('618547968046-anjfp0b6cfef8os0phsj1div5api5tgm.apps.googleusercontent.com')
},
{
  id: FacebookLoginProvider.PROVIDER_ID,
  provider:new FacebookLoginProvider('644485375953211')
}
],false);

export function provideConfig(){
  return config;
}



@NgModule({
  declarations:[
    AppComponent
  ],
  imports:[
    BrowserModule,
    SocialLoginModule
  ],
  providers: [
    {provide: AuthServiceConfig, useFactory:provideConfig}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }