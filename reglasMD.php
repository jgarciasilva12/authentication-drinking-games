<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reglasMD extends Model
{
    public $timestamps=false;
    protected $table ="reglas";//el nombre de la tabla de la base de datos
    protected  $fillable = array('Codigo','Nombre','Descripcion');
}