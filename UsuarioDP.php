<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsuarioDP extends Controller
{
    public function create(){
        $Usuario=new Usuario();
        $estado=Estado::all();
    }
    public function index(){
        $Usuario=Usuario::all();
    }
    public function store(Reques $request)
    {
        $Usuario=new Usuario();
        $Usuario->nombre=$request->nombre;
        $Usuario->correo=$request->correo;
        $Usuario->genero=$request->genero;
        $Usuario->edad=$request->edad;
        $Usuario->user_id = Auth::id();
        $Usuario->save();
        return Redirect::to('Usuario')->with('notice','Usuario guardado correctamente.');
    }
    public function show($id){
        $Usuario=Usuario::find($id);
    }
    public function edit($id)
    {
        $Usuario = Usuario::find($id);
        $estados = Estado::all();
    }
    public function update(Request $request, $id){
        $Usuario=Usuario::find($id);
        $Usuario->nombre=$request->nombre;
        $Usuario->correo=$request->correo;
        $Usuario->genero=$request->genero;
        $Usuario->edad=$request->edad;
        $Usuario->user_id = Auth::id();
        $Usuario->save();
        return Redirect::to('Usuario')->with('notice','Usuario guardado correctamente.');
    }
    public function destroy($id)
    {
        $Usuario = Usuario::find($id);
        $Usuario->delete();
    }
}
